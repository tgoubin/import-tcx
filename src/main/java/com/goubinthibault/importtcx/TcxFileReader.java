package com.goubinthibault.importtcx;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.goubinthibault.importtcx.model.TrainingCenterDatabase;

public class TcxFileReader {

	public TrainingCenterDatabase readTcxFile(String tcxFilePath) {
		try {
			String tcxFileContent = new String(Files.readAllBytes(Paths.get("src/main/resources/" + tcxFilePath)));
			return new XmlMapper().readValue(tcxFileContent, TrainingCenterDatabase.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
