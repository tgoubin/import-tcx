package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Creator {

	@JacksonXmlProperty(localName="xsi", isAttribute = true)
	private String xmlnsXsi;
	
	@JacksonXmlProperty(localName="type", isAttribute = true)
	private String xsiType;
	
	@JacksonXmlProperty(localName="Name")
	private String name;
	
	@JacksonXmlProperty(localName="UnitId")
	private Integer unitId;
	
	@JacksonXmlProperty(localName="ProductID")
	private Integer productID;
	
	@JacksonXmlProperty(localName="Version")
	private Version version;

	public String getXmlnsXsi() {
		return xmlnsXsi;
	}

	public void setXmlnsXsi(String xmlnsXsi) {
		this.xmlnsXsi = xmlnsXsi;
	}

	public String getXsiType() {
		return xsiType;
	}

	public void setXsiType(String xsiType) {
		this.xsiType = xsiType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public Integer getProductID() {
		return productID;
	}

	public void setProductID(Integer productID) {
		this.productID = productID;
	}

	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}
}
