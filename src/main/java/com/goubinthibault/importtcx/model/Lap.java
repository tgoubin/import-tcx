package com.goubinthibault.importtcx.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Lap {

	@JacksonXmlProperty(localName = "StartTime", isAttribute = true)
	private String startTime;

	@JacksonXmlProperty(localName = "TotalTimeSeconds")
	private BigDecimal totalTimeSeconds;

	@JacksonXmlProperty(localName = "DistanceMeters")
	private BigDecimal distanceMeters;

	@JacksonXmlProperty(localName = "MaximumSpeed")
	private BigDecimal maximumSpeed;

	@JacksonXmlProperty(localName = "Calories")
	private Integer calories;

	@JacksonXmlProperty(localName = "AverageHeartRateBpm")
	private HeartRate averageHeartRateBpm;

	@JacksonXmlProperty(localName = "MaximumHeartRateBpm")
	private HeartRate maximumHeartRateBpm;

	@JacksonXmlProperty(localName = "Intensity")
	private String intensity;

	@JacksonXmlProperty(localName = "Cadence")
	private Integer cadence;

	@JacksonXmlProperty(localName = "TriggerMethod")
	private String triggerMethod;

	@JacksonXmlProperty(localName = "Track")
	private Track track;

	@JacksonXmlProperty(localName = "Extensions")
	private Extensions extensions;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public BigDecimal getTotalTimeSeconds() {
		return totalTimeSeconds;
	}

	public void setTotalTimeSeconds(BigDecimal totalTimeSeconds) {
		this.totalTimeSeconds = totalTimeSeconds;
	}

	public BigDecimal getDistanceMeters() {
		return distanceMeters;
	}

	public void setDistanceMeters(BigDecimal distanceMeters) {
		this.distanceMeters = distanceMeters;
	}

	public BigDecimal getMaximumSpeed() {
		return maximumSpeed;
	}

	public void setMaximumSpeed(BigDecimal maximumSpeed) {
		this.maximumSpeed = maximumSpeed;
	}

	public Integer getCalories() {
		return calories;
	}

	public void setCalories(Integer calories) {
		this.calories = calories;
	}

	public HeartRate getAverageHeartRateBpm() {
		return averageHeartRateBpm;
	}

	public void setAverageHeartRateBpm(HeartRate averageHeartRateBpm) {
		this.averageHeartRateBpm = averageHeartRateBpm;
	}

	public HeartRate getMaximumHeartRateBpm() {
		return maximumHeartRateBpm;
	}

	public void setMaximumHeartRateBpm(HeartRate maximumHeartRateBpm) {
		this.maximumHeartRateBpm = maximumHeartRateBpm;
	}

	public String getIntensity() {
		return intensity;
	}

	public void setIntensity(String intensity) {
		this.intensity = intensity;
	}

	public Integer getCadence() {
		return cadence;
	}

	public void setCadence(Integer cadence) {
		this.cadence = cadence;
	}

	public String getTriggerMethod() {
		return triggerMethod;
	}

	public void setTriggerMethod(String triggerMethod) {
		this.triggerMethod = triggerMethod;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public Extensions getExtensions() {
		return extensions;
	}

	public void setExtensions(Extensions extensions) {
		this.extensions = extensions;
	}
}
