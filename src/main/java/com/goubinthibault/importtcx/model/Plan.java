package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Plan {

	@JacksonXmlProperty(localName = "Type", isAttribute = true)
	private String Type;

	@JacksonXmlProperty(localName = "IntervalWorkout", isAttribute = true)
	private String intervalWorkout;

	@JacksonXmlProperty(localName = "Extensions")
	private Extensions extensions;

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getIntervalWorkout() {
		return intervalWorkout;
	}

	public void setIntervalWorkout(String intervalWorkout) {
		this.intervalWorkout = intervalWorkout;
	}

	public Extensions getExtensions() {
		return extensions;
	}

	public void setExtensions(Extensions extensions) {
		this.extensions = extensions;
	}
}
