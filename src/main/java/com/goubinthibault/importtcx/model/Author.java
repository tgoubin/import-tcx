package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Author {

	@JacksonXmlProperty(localName = "xsi", isAttribute = true)
	private String xmlnsXsi;

	@JacksonXmlProperty(localName = "type", isAttribute = true)
	private String xsiType;

	@JacksonXmlProperty(localName = "Name")
	private String name;

	@JacksonXmlProperty(localName = "Build")
	private Build build;

	@JacksonXmlProperty(localName = "LangID")
	private String langID;

	@JacksonXmlProperty(localName = "PartNumber")
	private String partNumber;

	public String getXmlnsXsi() {
		return xmlnsXsi;
	}

	public void setXmlnsXsi(String xmlnsXsi) {
		this.xmlnsXsi = xmlnsXsi;
	}

	public String getXsiType() {
		return xsiType;
	}

	public void setXsiType(String xsiType) {
		this.xsiType = xsiType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Build getBuild() {
		return build;
	}

	public void setBuild(Build build) {
		this.build = build;
	}

	public String getLangID() {
		return langID;
	}

	public void setLangID(String langID) {
		this.langID = langID;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
}
