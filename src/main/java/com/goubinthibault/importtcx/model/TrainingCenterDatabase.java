package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class TrainingCenterDatabase {

	@JacksonXmlProperty(localName = "Activities")
	private Activities activities;

	@JacksonXmlProperty(localName = "Author")
	private Author author;

	public Activities getActivities() {
		return activities;
	}

	public void setActivities(Activities activities) {
		this.activities = activities;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}
}
