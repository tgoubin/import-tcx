package com.goubinthibault.importtcx.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Position {

	@JacksonXmlProperty(localName = "LatitudeDegrees")
	private BigDecimal latitudeDegrees;

	@JacksonXmlProperty(localName = "LongitudeDegrees")
	private BigDecimal longitudeDegrees;

	public BigDecimal getLatitudeDegrees() {
		return latitudeDegrees;
	}

	public void setLatitudeDegrees(BigDecimal latitudeDegrees) {
		this.latitudeDegrees = latitudeDegrees;
	}

	public BigDecimal getLongitudeDegrees() {
		return longitudeDegrees;
	}

	public void setLongitudeDegrees(BigDecimal longitudeDegrees) {
		this.longitudeDegrees = longitudeDegrees;
	}
}
