package com.goubinthibault.importtcx.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Trackpoint {

	@JacksonXmlProperty(localName = "Time")
	private String time;

	@JacksonXmlProperty(localName = "Position")
	private Position position;

	@JacksonXmlProperty(localName = "DistanceMeters")
	private BigDecimal distanceMeters;

	@JacksonXmlProperty(localName = "AltitudeMeters")
	private BigDecimal altitudeMeters;

	@JacksonXmlProperty(localName = "HeartRateBpm")
	private HeartRate heartRateBpm;

	@JacksonXmlProperty(localName = "Cadence")
	private Integer cadence;

	@JacksonXmlProperty(localName = "SensorState")
	private String sensorState;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public BigDecimal getDistanceMeters() {
		return distanceMeters;
	}

	public void setDistanceMeters(BigDecimal distanceMeters) {
		this.distanceMeters = distanceMeters;
	}

	public BigDecimal getAltitudeMeters() {
		return altitudeMeters;
	}

	public void setAltitudeMeters(BigDecimal altitudeMeters) {
		this.altitudeMeters = altitudeMeters;
	}

	public HeartRate getHeartRateBpm() {
		return heartRateBpm;
	}

	public void setHeartRateBpm(HeartRate heartRateBpm) {
		this.heartRateBpm = heartRateBpm;
	}

	public Integer getCadence() {
		return cadence;
	}

	public void setCadence(Integer cadence) {
		this.cadence = cadence;
	}

	public String getSensorState() {
		return sensorState;
	}

	public void setSensorState(String sensorState) {
		this.sensorState = sensorState;
	}
}
