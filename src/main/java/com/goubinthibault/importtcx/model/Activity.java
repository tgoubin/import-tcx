package com.goubinthibault.importtcx.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Activity {

	@JacksonXmlProperty(localName = "Id")
	private String id;

	@JacksonXmlProperty(localName = "Sport", isAttribute = true)
	private String sport;

	@JacksonXmlProperty(localName = "Lap")
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Lap> laps;

	@JacksonXmlProperty(localName = "Training")
	private Training training;

	@JacksonXmlProperty(localName = "Creator")
	private Creator creator;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public List<Lap> getLaps() {
		return laps;
	}

	public void setLaps(List<Lap> laps) {
		this.laps = laps;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public Creator getCreator() {
		return creator;
	}

	public void setCreator(Creator creator) {
		this.creator = creator;
	}
}
