package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Training {

	@JacksonXmlProperty(localName = "VirtualPartner", isAttribute = true)
	private String virtualPartner;

	@JacksonXmlProperty(localName = "Plan")
	private Plan plan;

	public String getVirtualPartner() {
		return virtualPartner;
	}

	public void setVirtualPartner(String virtualPartner) {
		this.virtualPartner = virtualPartner;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}
}
