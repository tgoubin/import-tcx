package com.goubinthibault.importtcx.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Activities {

	@JacksonXmlProperty(localName="Activity")
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Activity> activities;

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivity(List<Activity> activities) {
		this.activities = activities;
	}
}
