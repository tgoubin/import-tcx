package com.goubinthibault.importtcx.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class LX {

	@JacksonXmlProperty(localName = "xmlns", isAttribute = true)
	private String xmlns;

	@JacksonXmlProperty(localName = "AvgSpeed")
	private BigDecimal avgSpeed;

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public BigDecimal getAvgSpeed() {
		return avgSpeed;
	}

	public void setAvgSpeed(BigDecimal avgSpeed) {
		this.avgSpeed = avgSpeed;
	}
}
