package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Extensions {

	@JacksonXmlProperty(localName = "LX")
	private LX lx;

	public LX getLx() {
		return lx;
	}

	public void setLx(LX lx) {
		this.lx = lx;
	}
}
