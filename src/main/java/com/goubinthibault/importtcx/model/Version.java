package com.goubinthibault.importtcx.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Version {

	@JacksonXmlProperty(localName = "VersionMajor")
	private Integer versionMajor;

	@JacksonXmlProperty(localName = "VersionMinor")
	private Integer versionMinor;

	@JacksonXmlProperty(localName = "BuildMajor")
	private Integer buildMajor;

	@JacksonXmlProperty(localName = "BuildMinor")
	private Integer buildMinor;

	public Integer getVersionMajor() {
		return versionMajor;
	}

	public void setVersionMajor(Integer versionMajor) {
		this.versionMajor = versionMajor;
	}

	public Integer getVersionMinor() {
		return versionMinor;
	}

	public void setVersionMinor(Integer versionMinor) {
		this.versionMinor = versionMinor;
	}

	public Integer getBuildMajor() {
		return buildMajor;
	}

	public void setBuildMajor(Integer buildMajor) {
		this.buildMajor = buildMajor;
	}

	public Integer getBuildMinor() {
		return buildMinor;
	}

	public void setBuildMinor(Integer buildMinor) {
		this.buildMinor = buildMinor;
	}
}