package com.goubinthibault.importtcx.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Track {

	@JacksonXmlProperty(localName = "Trackpoint")
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Trackpoint> trackpoints;

	public List<Trackpoint> getTrackpoints() {
		return trackpoints;
	}

	public void setTrackPoints(List<Trackpoint> trackpoints) {
		this.trackpoints = trackpoints;
	}
}
