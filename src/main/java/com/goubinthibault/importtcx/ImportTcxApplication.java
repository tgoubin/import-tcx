package com.goubinthibault.importtcx;

import com.goubinthibault.importtcx.model.TrainingCenterDatabase;

public class ImportTcxApplication {

	public static void main(String[] args) {
		TcxFileReader tcxFileReader = new TcxFileReader();

		TrainingCenterDatabase swimming = tcxFileReader.readTcxFile("tcx/swimming.tcx");
		TrainingCenterDatabase cycling = tcxFileReader.readTcxFile("tcx/cycling.tcx");
		TrainingCenterDatabase running = tcxFileReader.readTcxFile("tcx/running.tcx");
	}
}
